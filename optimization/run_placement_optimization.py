import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from pymoo.optimize import minimize
from pymoo.factory import get_algorithm, get_crossover, get_mutation, get_sampling, get_termination

from utilities import path, utils
from optimization.custom_callback import CustomeCallback
from optimization.placement_problem import PlacementProblem

# Read trace WNTR data
n_gen = 500
n_obj = path.n_obj
network_name = path.network_name
trace_matrix = pd.read_csv(path.simulation_results_dir + network_name + '_trace.csv', header=0, sep=',')
sensors = list(trace_matrix['Node'].unique())

# Algorithm initialization
algorithm = get_algorithm('nsga2',
                          pop_size=40,
                          n_offsprings=10,
                          sampling=get_sampling("int_random"),
                          crossover=get_crossover("int_sbx", prob=1, eta=3.0),
                          mutation=get_mutation("int_pm", eta=3.0),
                          eliminate_duplicates=True)

# Termination criterion definition
termination = get_termination("n_gen", n_gen)

# Optimize placements with different sensor budgets
p_median_values_nga = []
p_median_placements_nga = []
p_median_placements_brute_force = []
p_median_points_nga = []
print('#######################################################')
time_start = time.time()
for index, sensor in enumerate(sensors):
    time_intermediate_start = time.time()
    sensor_budget = index + 25
    print('Experiment', sensor_budget, 'out of', len(sensors))
    print('#######################################################')
    problem = PlacementProblem(network_name=network_name,
                               trace_WNTR_matrix=trace_matrix,
                               n_gen=n_gen,
                               n_obj=n_obj,
                               sensor_budget=sensor_budget)
    if n_obj == 1:
        res = minimize(problem, algorithm, termination,
                       seed=1,
                       save_history=True,
                       verbose=path.verbose)
    else:
        res = minimize(problem, algorithm, termination,
                       callback=CustomeCallback(),
                       seed=1,
                       save_history=True,
                       verbose=path.verbose)
        utils.save_optimization_history(res=res, sensor_budget=sensor_budget)
    print('Sensor budget:\t', sensor_budget)
    print('-------------------------------------------------------')
    placements, optimal_points, optimal_f = problem.get_optimal_solution(res)
    min_index = np.argmin(optimal_f)
    p_median_values_nga.append(optimal_f[min_index])
    p_median_placements_nga.append(placements[min_index])
    p_median_points_nga.append(optimal_points[min_index])
    print('>>> NSGA II')
    print('Placement:\n', placements[min_index])
    print('Point:\n', optimal_points[min_index])
    print('Value:\n', optimal_f[min_index])
    print('-------------------------------------------------------')
    if path.brute_force:
        problem.brute_force_opt(res=res)
    if n_obj == 2:
        problem.plot_biobj_solution(res=res, save_path=path.biobjective_plot_dir)
        problem.plot_hypervolume(res=res, save_path=path.biobjective_plot_dir)
        problem.plot_hypervolume_per_gen(res=res, save_path=path.biobjective_plot_dir)
        problem.plot_homogeneity_per_gen(res=res, save_path=path.biobjective_plot_dir)
    time_intermediate_stop = time.time() - time_intermediate_start
    print('Intermediate time:', time_intermediate_stop)
    print('#######################################################')
time_stop = time.time() - time_start
print('Stop time:', time_stop)

# Single Objective
if n_obj == 1:
    # TODO refactor the save of the final results in a csv
    # pmedian_results = pd.DataFrame(list(zip(p_median_points_nga, p_median_placements_nga, p_median_values_nga,
    #                                         p_median_placements_brute_force)),
    #                                columns=['Point_NGA_II', 'Placement_NGA_II', 'Value_NGA_II',
    #                                         'All_Placement_Brute_Force'])
    # pmedian_results.to_csv(path.optimization_results_dir + network_name + '_pmedian_placements.csv',
    #                        sep=';', index=False)
    # Plot results
    np.save(file=path.singleobjective_plot_dir + network_name + '_single_obj_det_times',
            arr=np.array(p_median_values_nga))
    plt.plot(range(1, len(p_median_values_nga) + 1), p_median_values_nga,
             linewidth=2, color='lightblue',
             marker='.', mfc='blue', ms=8, mec='darkblue')
    plt.title(network_name)
    plt.xlabel('Sensor Budget')
    step = (len(p_median_values_nga) + 1) % 10
    plt.xticks(np.arange(1, len(p_median_values_nga) + 1, step=step))
    plt.ylabel('Time')
    plt.savefig(path.singleobjective_plot_dir + network_name + '_single_obj_det_times.png',
                transparent=path.transparent)
    plt.show()
