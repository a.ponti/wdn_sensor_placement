import itertools
import time
import numpy as np
import pandas as pd
from scipy.spatial import distance

from pymoo.model.callback import Callback
from pymoo.performance_indicator.hv import Hypervolume

from quality_simulation.extract_data_from_WNTR import extract_placement_micro_array
from utilities import utils, path


class CustomeCallback(Callback):

    def __init__(self) -> None:
        super().__init__()
        # Init data that have to be tracked
        self.data_keys = ['HV', 'Edit', 'Frobenius', 'NParetoSol',
                          'MaxMean', 'MinMean', 'MaxSTD', 'MinSTD',
                          'MaxMeanFeasible', 'MinMeanFeasible', 'MaxSTDFeasible', 'MinSTDFeasible']
        for key in self.data_keys:
            self.data[key] = []

        # Init usefull variable to optimize the computations
        self.trace_WNTR_matrix = pd.read_csv(path.simulation_results_dir + path.network_name + '_trace.csv',
                                             header=0, sep=',')
        self.scenarios = list(self.trace_WNTR_matrix.columns)[0:-2]
        self.sensors = list(self.trace_WNTR_matrix['Node'].unique())
        # Pre-load all micro-arrays
        self.micro_arrays = {}
        for sensor in self.sensors:
            self.micro_arrays[sensor] = pd.read_csv(
                path.sensor_concentration_micro_arrays_dir + 'concentration_sensor_' + str(sensor) + '.csv',
                header=0, decimal=',', sep=';')

    def notify(self, algorithm):
        start = time.time()
        # Save hypervolume and number of unique paretian solution per generation
        self.hypervolume(algorithm)
        # Save min and max values of mean and std per generation
        self.min_max_values(algorithm)
        # Save distances in the design (hamming) and matrix (frobenius) spaces
        # self.hamming_design_space(algorithm)
        # self.frobenius_matrix_space(algorithm)
        # self.print_generation()
        print('Computation Time:', time.time() - start)

    def min_max_values(self, algorithm):
        # Get the feasible population values of this generation
        feas = np.where(algorithm.pop.get('feasible'))[0]
        F = np.array(algorithm.pop.get('F'))
        F_feasible = F[feas]
        # Divide mean and std values of all solutions and only feasible solutions
        mean_F = [x[0] for x in F]
        std_F = [x[1] for x in F]
        mean_feasible_F = [x[0] for x in F_feasible]
        std_feasible_F = [x[1] for x in F_feasible]
        self.data['MaxMean'].append(max(mean_F))
        self.data['MinMean'].append(min(mean_F))
        self.data['MaxSTD'].append(max(std_F))
        self.data['MinSTD'].append(min(std_F))
        # If there aren't feasible solution the max/min mean/std are NaN
        if len(mean_feasible_F) > 0:
            max_mean_feasible_F, min_mean_feasible_F = max(mean_feasible_F), min(mean_feasible_F)
            max_std_feasible_F, min_std_feasible_F = max(std_feasible_F), min(std_feasible_F)
        else:
            max_mean_feasible_F, min_mean_feasible_F = np.nan, np.nan
            max_std_feasible_F, min_std_feasible_F = np.nan, np.nan
        self.data['MaxMeanFeasible'].append(max_mean_feasible_F)
        self.data['MinMeanFeasible'].append(min_mean_feasible_F)
        self.data['MaxSTDFeasible'].append(max_std_feasible_F)
        self.data['MinSTDFeasible'].append(min_std_feasible_F)

    def hypervolume(self, algorithm):
        # Get pareto-efficient values
        F = np.array(algorithm.pop.get('F'))
        pareto_index = utils.identify_pareto(F)
        pareto_front = F[pareto_index]

        # Compute the hyper-volume
        ref_point = (86400, 86400 / 2)
        metric = Hypervolume(ref_point=ref_point, normalize=False)
        hv = metric.calc(pareto_front)
        self.data['HV'].append(hv)
        # Get number of unique paretian solutions
        unique_data = [list(x) for x in set(tuple(x) for x in pareto_front)]
        self.data['NParetoSol'].append(len(unique_data))

    def hamming_design_space(self, algorithm):
        # Get the feasible population of this generation
        feas = np.where(algorithm.pop.get('feasible'))[0]
        X = np.array(algorithm.pop.get('X'))[feas].tolist()
        # Compute kappa max
        n_pop = len(X)
        kappa_max = ((n_pop * n_pop) - n_pop) / 2
        hamming_distances = []
        if len(X) > 1:
            # Create a list of all possible pair of individuals
            X_combinations = list(itertools.combinations(X, 2))
            # Compute Edit distance between all pairs of individuals
            for X_pair in X_combinations:
                hamming_distances.append(distance.hamming(u=X_pair[0], v=X_pair[1]))
            # Normalize all distances
            norm_hamming_distances = utils.normalize(hamming_distances)
            # Sum all normalized distances
            kappa_hamming = sum(norm_hamming_distances)
            # Compute Kappa
            kappa_hamming = kappa_hamming / kappa_max
        else:
            kappa_hamming = np.nan
        # Save this measure in the final results given by the optimization process
        self.data['Edit'].append(kappa_hamming)

    def frobenius_matrix_space(self, algorithm):
        # Get the feasible population of this generation
        feas = np.where(algorithm.pop.get('feasible'))[0]
        X = algorithm.pop.get('X')[feas]
        # Compute kappa max
        n_pop = len(X)
        kappa_max = ((n_pop * n_pop) - n_pop) / 2
        # Get the placements in terms of sensors name (e.g. [12, 32]) from a solution (e.g. [1, 0, ..., 1, 0])
        placements = utils.binary_vector_to_placement(X, self.sensors)
        # Get all possible pairs of placements
        placements_combinations = list(itertools.combinations(placements, 2))
        frobenius_distances = []
        if len(X) > 1:
            for placement_pair in placements_combinations:

                if len(placement_pair[0]) != 0:
                    # Get the micro array of this placement
                    micro_array_1 = extract_placement_micro_array(self.trace_WNTR_matrix, placement_pair[0],
                                                                  micro_arrays=self.micro_arrays, plot=False)
                    # Convert pandas dataframe in numpy 2D array
                    micro_array_1 = micro_array_1.to_numpy()
                else:
                    # If the placement is empty (no sensors are placed) the micro array matrix is full of 86400
                    # (the maximum simulation time)
                    # TODO 25 is the number of time step and it should be parametrized
                    micro_array_1 = np.full(shape=(25, len(self.scenarios)), fill_value=0)

                if len(placement_pair[1]) != 0:
                    # Get the micro array of this placement
                    micro_array_2 = extract_placement_micro_array(self.trace_WNTR_matrix, placement_pair[1],
                                                                  micro_arrays=self.micro_arrays, plot=False)
                    # Convert pandas dataframe in numpy 2D array
                    micro_array_2 = micro_array_2.to_numpy()
                else:
                    # If the placement is empty (no sensors are placed) the micro array matrix is full of 86400
                    # (the maximum simulation time)
                    micro_array_2 = np.full(shape=(25, len(self.scenarios)), fill_value=0)
                # Compute the Frobenius distance between this two matrix
                difference = micro_array_1 - micro_array_2
                frobenius_distances.append(np.linalg.norm(difference, ord='fro'))
            # Normalize all distances
            norm_frobenius_distances = utils.normalize(frobenius_distances)
            # Sum all normalized distances
            kappa_frobenius = sum(norm_frobenius_distances)
            # Compute Kappa
            kappa_frobenius = kappa_frobenius / kappa_max
        else:
            # If two or less individuals of population are feasible the kappa is Nan
            kappa_frobenius = np.nan
        # Save this measure in the final results given by the optimization process
        self.data['Frobenius'].append(kappa_frobenius)

    def print_generation(self):
        for key in self.data_keys:
            print(key, ': ', self.data[key][len(self.data[key]) - 1], sep='')
