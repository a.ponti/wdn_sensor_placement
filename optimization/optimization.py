import pandas as pd

from pymoo.optimize import minimize
from pymoo.factory import get_algorithm, get_crossover, get_mutation, get_sampling, get_termination

from optimization.placement_problem import PlacementProblem
from utilities import path

print('Reading data...')
network_name = path.network_name
trace_matrix = pd.read_csv(path.simulation_results_dir + network_name + '_trace.csv', header=0, sep=',')

print('Defining the problem...')
problem = PlacementProblem(network_name=network_name,
                           trace_WNTR_matrix=trace_matrix,
                           n_obj=2,
                           sensor_budget=4)

# Algorithm initialization
print('Initializing the algorithm...')
algorithm = get_algorithm('nsga2',
                          pop_size=40,
                          n_offsprings=10,
                          sampling=get_sampling("int_random"),
                          crossover=get_crossover("int_sbx", prob=1, eta=3.0),
                          mutation=get_mutation("int_pm", eta=3.0),
                          eliminate_duplicates=True
                          )

# Termination criterion definition
print('Defining the termination criterion...')
termination = get_termination("n_gen", 50)

# Optimization
print('Optimizing...')
res = minimize(problem,
               algorithm,
               termination,
               seed=1,
               save_history=True,
               verbose=True)

# placements, optimal_x, optimal_f = problem.get_optimal_solution(res.history)
#
# print('Placements:\t', placements)
# print('Points:\t', optimal_x)
# print('Values:\t', optimal_f)

# # Results visualization
# # TODO Improve the results visualization (show directly the choosen sensors names)
# print('\n---------------------------------------------------------------------')
# print('Scenarios:\t', problem.get_scenarios())
# print('Sensors:\t', problem.get_sensors())
# print('---------------------------------------------------------------------')
# print('>>> NGA II algorithm:')
# print('---------------------------------------------------------------------')
# for index in range(len(res.F)):
#     print('Result:\t\t', res.F[index][0])
#     print('Placement:\t', list(res.X[index]))
#
# # Solve the problem trying all the possible solutions
# min_value, solution, x = problem.brute_force_opt()
# print('---------------------------------------------------------------------')
# print('>>> Brute Force algorithm:')
# print('---------------------------------------------------------------------')
# print('Result:\t\t', min_value)
# print('Placement:\t', solution)
# print('---------------------------------------------------------------------')
