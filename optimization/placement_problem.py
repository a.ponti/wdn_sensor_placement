import itertools
import time

import numpy as np
import pandas as pd
from scipy.spatial import distance

np.seterr(divide='ignore', invalid='ignore')
import matplotlib.pyplot as plt

from pathlib import Path
from pymoo.model.problem import Problem
from pymoo.performance_indicator.hv import Hypervolume

from utilities import utils, path
from quality_simulation.extract_data_from_WNTR import extract_detection_times


class PlacementProblem(Problem):

    def __init__(self, network_name, trace_WNTR_matrix, n_gen=500, n_obj=1, sensor_budget=1, sensor_sensibility=10):
        """
        :param network_name: name of the network to analyse.
        :param trace_WNTR_matrix: trace matrix resulting from the WNTR water quality simulation.
        :param n_obj: number of objective functions to use.
        If 1 use the p_median function, if 2 use the p_median and variance functions.
        :param sensor_budget: maximum number of sensors to place.
        :param sensor_sensibility: level of concentration that sensors can detect (in [1,100])
        """
        self.network_name = network_name
        self.trace_WNTR_matrix = trace_WNTR_matrix
        self.sensor_budget = sensor_budget
        self.sensor_sensibility = sensor_sensibility
        self.n_gen = n_gen
        self.n_obj = n_obj

        # Extract scenarios and sensors list considered in the WNTR simulation
        # Considering the case in which sensors and scenarios are the same list of nodes
        self.scenarios = list(self.trace_WNTR_matrix.columns)[0:-2]
        self.sensors = list(self.trace_WNTR_matrix['Node'].unique())

        # Extract detection times matrix from WNTR simulation data
        det_times_path = path.optimization_results_dir + path.network_name + '_det_times_numpy.npy'
        if not (Path(det_times_path).is_file()):
            self.det_times = extract_detection_times(trace_WNTR_matrix=trace_WNTR_matrix,
                                                     sensor_sensibility=sensor_sensibility,
                                                     save_path=path.optimization_results_dir)
        else:
            self.det_times = np.load(file=det_times_path).tolist()

        # NB:   Only for computation purpose you can remove sensor that never detect any contamination.
        #       That's results in faster computation.
        # self.remove_useless_sensors()

        # One boolean variable for each sensor
        n_var = len(self.sensors)
        n_constr = 1
        super().__init__(n_var=n_var,
                         n_obj=n_obj,
                         n_constr=n_constr,
                         xl=np.full(n_var, 0),
                         xu=np.full(n_var, 1),
                         elementwise_evaluation=True,
                         type_var=np.int)

    def _evaluate(self, variables, out, *args, **kwargs):
        s = variables

        f = [self.mean_function(s)]
        if self.n_obj > 1:
            f.append(self.std_function(s))

        # Considering that the x matrix is automatically computed from the solution,
        # the only necessary constraints is the one that defines the sensor budget
        g = [self.constraint_sensor_budget(s)]
        # g.extend(self.constraints_active_sensors(s, x))
        # g.extend(self.constraint_one_sensor_for_scenario(x))

        out['F'] = f
        out['G'] = g

    def extract_x_matrix_from_solution(self, solution):
        """
        Extract which sensor should be active based on the solution parameter.
        For each scenario, the active sensor should be the one active in the solution with the minimum detection time.
        """
        # Get active sensors from solution
        active_sensors = [index for index, sensor in enumerate(solution) if sensor == 1]
        x = []
        for scenarios_index in range(len(self.scenarios)):
            scenario_row = list(np.zeros(len(self.sensors), dtype=np.int))
            min_det_tmp = 100000
            min_sensor_index = 0
            # Check which sensor out of the active sensor of the solution should be active in this scenario
            for sensor_index in active_sensors:
                det_time_tmp = self.det_times[scenarios_index][sensor_index]
                if (det_time_tmp >= 0) & (det_time_tmp <= min_det_tmp):
                    min_det_tmp = det_time_tmp
                    min_sensor_index = sensor_index
            scenario_row[min_sensor_index] = 1
            x.append(scenario_row)

        return x

    def mean_function(self, s):
        """
        Compute the objective function (p-median) described in (pag.9)
        [Berry, J., Hart, W. E., Phillips, C. A., Uber, J. G., & Watson, J. P. (2006).
        Sensor placement in municipal water networks with temporal integer programming models.
        Journal of water resources planning and management, 132(4), 218-224.]

        :param s:
        :return: sum_(a in A) [ alpha_a * sum_(i in L_a) ( d_(ai) * x_(ai) ) ]
        """
        if sum(s) == 0:
            return 86400
        result = 0
        x = self.extract_x_matrix_from_solution(s)
        for scenario_index in range(len(self.scenarios)):
            tmp_sum = 0
            for sensor_index in range(len(self.sensors)):
                # Take the impact for the couple scenario-sensor
                d_scenario_sensor = self.det_times[scenario_index][sensor_index]
                x_scenario_sensor = x[scenario_index][sensor_index]
                # Sum of impact of the active sensors
                tmp_sum += d_scenario_sensor * x_scenario_sensor
            # Lets consider that all the scenarios have the same probability
            # TODO Probability of each scenario can be parametrized
            result += tmp_sum / len(self.scenarios)
        return result

    def std_function(self, s):
        # To modelling the variance, the x_ai, for each scenario a, are 1 in all the
        # active sensors positions indicated by the actual solution.
        # The constraints relative to the sum of x_ai must be removed for this formulation.
        #  NB: These constraints are no longer necessary even for the mean function.
        #  NB: That's mean that we can use directly s_i instead of x_ai.
        if sum(s) == 0:
            return 0
        result = 0
        for scenario_index in range(len(self.scenarios)):
            sensors_det_times = []
            for sensor_index in range(len(self.sensors)):
                # Take the impact for the couple scenario-sensor
                d_scenario_sensor = self.det_times[scenario_index][sensor_index]
                # If this sensor is active in the solution, his detection time must be considered computing variance
                if s[sensor_index] == 1:
                    sensors_det_times.append(d_scenario_sensor)
            # Lets consider that all the scenarios have the same probability
            # TODO Probability of each scenario can be parametrized
            tmp_var = np.std(sensors_det_times)
            result += tmp_var / len(self.scenarios)
        return result

    def constraints_active_sensors(self, s, x):
        """
        A sensor cannot raise an alarm if there is no sensor installed there.

        :param s: Binary decision variable, for each potential sensor location i
        is equals to 1 if a sensor is placed at location i and 0 otherwise.
        :param x: Binary matrix that rapresent active sensors per scenario.
        :return: Set of inequality to ensure this constraints (x_(ai) <= s_(i)).
        """

        g = []
        for scenario_index in range(len(self.scenarios)):
            for sensor_index in range(len(self.sensors)):
                x_scenario_sensor = x[scenario_index][sensor_index]
                g_tmp = x_scenario_sensor - s[sensor_index]
                # norm_coeff = s[sensor_index]
                # g_tmp /= norm_coeff
                g.append(g_tmp)
        return g

    def constraint_sensor_budget(self, s):
        """
        There is a maximum number of sensors that can be installed.

        :param s: Binary decision variable, for each potential sensor location i
        is equals to 1 if a sensor is placed at location i and 0 otherwise.
        :return: Inequality to ensure this constraint (sum_(i in L_a) <= p).
        """
        sensor_number = 0
        # norm_coeff = 1
        for sensor_index in range(len(self.sensors)):
            sensor_number += s[sensor_index]
            # norm_coeff *= (-self.sensor_budget)
        sensor_number -= self.sensor_budget
        # sensor_number /= norm_coeff
        # sensor_number = abs(sensor_number)  # Make an equality constraint
        return sensor_number

    def constraint_one_sensor_for_scenario(self, x):
        """
        In each scenario there would be exactly one sensor installed.
        """
        g = []
        for scenario_index in range(len(self.scenarios)):
            sensor_sum = 0
            for sensor_index in range(len(self.sensors)):
                sensor_sum += x[scenario_index][sensor_index]
            g_tmp = abs(sensor_sum - 1)
            g.append(g_tmp)
        return g

    def get_optimal_solution(self, res):
        if self.n_obj == 1:
            placements, optimal_points, optimal_f = self.get_single_obj_optimal_solution(res.history)
        else:
            placements, optimal_points, optimal_f = self.get_bi_obj_optimal_solution(res)
        return placements, optimal_points, optimal_f

    def get_single_obj_optimal_solution(self, history):
        # Get all tryed solution and select the feasible ones
        points = np.row_stack([a.pop.get("X") for a in history])
        f = np.row_stack([a.pop.get("F") for a in history])
        feasible = list(np.row_stack([a.pop.get("feasible") for a in history])[:, 0])
        # Get indexes of feasible solutions
        feasible_indexes = [i for i, x in enumerate(feasible) if x]
        # Get feasible solutions (points and values)
        feasible_points = points[feasible_indexes].tolist()
        feasible_f = f[feasible_indexes].flatten().tolist()
        # Get indexes of optimal solutions
        optimal_indexes = np.argwhere(feasible_f == np.amin(feasible_f))
        optimal_indexes = optimal_indexes.flatten().tolist()

        # Get optimal points and values
        optimal_points = np.array(feasible_points)[optimal_indexes].tolist()
        optimal_points, indexes_to_keep = utils.remove_duplicates_list_of_lists(optimal_points)
        optimal_f = np.array(feasible_f)[optimal_indexes].tolist()
        optimal_f = np.array(optimal_f)[indexes_to_keep].tolist()

        # Associate points with corresponding sensors names
        placements = []
        for solution in optimal_points:
            sensor_indexes = [i for i, sensor in enumerate(solution) if sensor]
            placements.append(np.array(self.sensors)[sensor_indexes].tolist())

        return placements, optimal_points, optimal_f

    def get_bi_obj_optimal_solution(self, res):
        optimal_points = res.X
        optimal_f = res.F
        placements = []
        for solution in optimal_points:
            sensor_indexes = [i for i, sensor in enumerate(solution) if sensor]
            placements.append(np.array(self.sensors)[sensor_indexes].tolist())
        return placements, optimal_points, optimal_f

    def plot_biobj_solution(self, res, save_path=''):
        # Get list of optimal solutions
        res_F = res.F.tolist()
        # Get all solutions
        f = np.row_stack([a.pop.get("F") for a in res.history])
        feasible = list(np.row_stack([a.pop.get("feasible") for a in res.history])[:, 0])
        # Get indexes of feasible solutions
        feasible_indexes = [i for i, x in enumerate(feasible) if x]
        # Get feasible solutions (values)
        feasible_f = f[feasible_indexes].tolist()
        # Remove duplicates
        feasible_f.sort()
        feasible_f = list(lst for lst, _ in itertools.groupby(feasible_f))
        # Remove optimal solutions from feasible (for graphic purpose)
        feasible_f = [lst for lst in feasible_f if lst not in res_F]

        # Scatter plot
        mean = [x[0] for x in feasible_f]
        var = [x[1] for x in feasible_f]
        res_F = sorted(res_F, key=lambda x: x[0])
        optimal_mean = [x[0] for x in res_F]
        optimal_var = [x[1] for x in res_F]

        plt.plot(mean, var, '.', color='lightgrey')
        plt.step(optimal_mean, optimal_var, where='post', label='post', color='red', alpha=0.3)
        plt.plot(optimal_mean, optimal_var, 'C0D', color='red', ms=3)

        plt.xlim(-1000, 90000)
        plt.ylim(-1000, 45000)
        plt.title(path.network_name + "\nObjective Space (Sensor Budget = " + str(self.sensor_budget) + ")")
        plt.xlabel('mean')
        plt.ylabel('std')

        if save_path != '':
            plt.savefig(save_path + self.network_name + '_objective_space_budget_' + str(self.sensor_budget) + '.png',
                        transparent=path.transparent)
        plt.show()

    def plot_hypervolume(self, res, save_path=''):
        # Get list of optimal solutions
        res_F = res.F.tolist()

        # Scatter plot
        res_F = sorted(res_F, key=lambda x: x[0])
        optimal_mean = [x[0] for x in res_F]
        optimal_std = [x[1] for x in res_F]
        # TODO The reference point should be automatic calculated considering the maximum simulation time
        ref_point = (86400, 86400 / 2)
        # Compute the hyper-volume
        metric = Hypervolume(ref_point=ref_point, normalize=False)
        hv = metric.calc(res.F)
        plt.text(ref_point[0] - 30000, ref_point[1] - 3000, 'HV = ' + str(int(hv)), color='grey')
        # Get min and max of mean and std to create the shape of hyper-volume
        min_mean = min(optimal_mean)
        max_mean = max(optimal_mean)
        min_std = min(optimal_std)
        max_std = max(optimal_std)

        line_color = 'grey'
        # Line for std
        x_coords = [min_mean, min_mean]
        y_coords = [max_std, ref_point[1]]
        line1 = plt.plot(x_coords, y_coords, color=line_color, alpha=0.3)
        # Line for mean
        x_coords = [max_mean, ref_point[0]]
        y_coords = [min_std, min_std]
        line2 = plt.plot(x_coords, y_coords, color=line_color, alpha=0.3)
        # Line to close the polygon
        x_coords = [min_mean, ref_point[0]]
        y_coords = [ref_point[1], ref_point[1]]
        line3 = plt.plot(x_coords, y_coords, color=line_color, alpha=0.3)
        x_coords = [ref_point[0], ref_point[0]]
        y_coords = [min_std, ref_point[1]]
        line4 = plt.plot(x_coords, y_coords, color=line_color, alpha=0.3)

        step_line = plt.step(optimal_mean, optimal_std, where='post', label='post', color='red', alpha=0.3)
        plt.plot(optimal_mean, optimal_std, 'C0.', color='red', ms=4)
        plt.plot(ref_point[0], ref_point[1], 'C0*', color='darkgrey', ms=4)
        plt.plot()

        plt.xlim(-1000, 90000)
        plt.ylim(-1000, 45000)
        plt.title(path.network_name + "\nHyper-Volume (Sensor Budget = " + str(self.sensor_budget) + ")")
        plt.xlabel('mean')
        plt.ylabel('std')

        if save_path != '':
            plt.savefig(save_path + self.network_name + '_hypervolume_budget_' + str(self.sensor_budget) + '.png',
                        transparent=path.transparent)
        plt.show()

    def plot_hypervolume_per_gen(self, res, save_path=''):
        plt.plot(range(1, self.n_gen + 1), res.algorithm.callback.data['HV'])
        plt.xlabel('Generation')
        plt.ylabel('Hyper-Volume')
        plt.title(path.network_name + "\nHyper-Volume per generation (Sensor Budget = " + str(self.sensor_budget) + ")")

        if save_path != '':
            plt.savefig(save_path + self.network_name + '_hypervolume_generation_budget_' +
                        str(self.sensor_budget) + '.png',
                        transparent=path.transparent)
        plt.show()

    def plot_homogeneity_per_gen(self, res, save_path=''):
        plt.plot(range(1, self.n_gen + 1), res.algorithm.callback.data['Edit'], label='Edit')
        plt.plot(range(1, self.n_gen + 1), res.algorithm.callback.data['Frobenius'], label='Frobenius')
        plt.xlabel('Generation')
        plt.ylabel('Edit Distance')
        plt.legend()
        plt.title(path.network_name +
                  "\nHomogeneity (Sensor Budget = " + str(self.sensor_budget) + ")")

        if save_path != '':
            plt.savefig(save_path + self.network_name + '_homogeneity_generation_budget_' +
                        str(self.sensor_budget) + '.png',
                        transparent=path.transparent)
        plt.show()

    def brute_force_opt(self, res):
        # Compute all possible combinations of sensor placement
        possible_solutions = [list(sol) for sol in itertools.product([0, 1], repeat=len(self.sensors))]
        # Keep only the combination with maximum 'sensor_budget' active sensor
        possible_solutions = [sol for sol in possible_solutions if sum(sol) <= self.sensor_budget]

        values = []
        for solution in possible_solutions:
            mean = self.mean_function(solution)
            std = self.std_function(solution)
            values.append([mean, std])
        pareto_index = utils.identify_pareto(np.array(values))
        pareto_values = np.array(values)[pareto_index].tolist()

        # Scatter plot BRUTE FORCE
        pareto_values = sorted(pareto_values, key=lambda x: x[0])
        optimal_mean = [x[0] for x in pareto_values]
        optimal_std = [x[1] for x in pareto_values]

        utils.plot_hypervolume_lines(optimal_mean, optimal_std, line_color='lightblue', line_width=3)

        plt.step(optimal_mean, optimal_std, where='post', color='lightblue', linewidth=3)
        plt.plot(optimal_mean, optimal_std, 'C0D', color='blue', ms=4, label='Brute Force')

        # Scatter plot NSGA II
        # Get list of optimal solutions
        res_F = res.F.tolist()

        # Scatter plot
        res_F = sorted(res_F, key=lambda x: x[0])
        optimal_mean = [x[0] for x in res_F]
        optimal_std = [x[1] for x in res_F]

        utils.plot_hypervolume_lines(optimal_mean, optimal_std, line_color='red', line_width=1)

        plt.step(optimal_mean, optimal_std, where='post', color='red', linewidth=1)
        plt.plot(optimal_mean, optimal_std, 'C0D', color='red', ms=2, label='NSGA II')

        plt.xlim(-1000, 90000)
        plt.ylim(-1000, 45000)

        plt.legend()

        plt.title(path.network_name + "\n Objective Space (Sensor Budget <= " +
                  str(self.sensor_budget) + ")")
        plt.xlabel('mean')
        plt.ylabel('std')
        plt.show()



    def brute_force_opt_distances(self):
        # Compute all possible combinations of sensor placement
        possible_solutions = [list(sol) for sol in itertools.product([0, 1], repeat=len(self.sensors))]
        # Keep only the combination with maximum 'sensor_budget' active sensor
        possible_solutions = [sol for sol in possible_solutions if sum(sol) <= self.sensor_budget]

        solutions_pair = list(itertools.combinations(possible_solutions, 2))

        s1, s2 = [], []
        n1, n2 = [], []
        mean_value_1, mean_value_2 = [], []
        std_value_1, std_value_2 = [], []
        hamming_dist, frobenius_dist = [], []
        index = 1
        max_index = len(solutions_pair)
        start = time.time()
        for solution in solutions_pair:
            if index % 1000 == 0:
                print(solution[0])
                print(len(solution[0]))
                print('Iteration', index, 'out of', max_index, '[', time.time(), ']')
            index += 1
            s1_placement = utils.binary_solution_to_placement(solution[0], self.sensors)
            s2_placement = utils.binary_solution_to_placement(solution[1], self.sensors)
            # print('SOLUTION', s1_placement, '-', s2_placement)
            # Save the pair of placement
            s1.append('-'.join([str(elem) for elem in s1_placement]))
            s2.append('-'.join([str(elem) for elem in s2_placement]))
            # Save the number of sensor in each placement
            n1.append(sum(solution[0]))
            n2.append(sum(solution[1]))
            # Save mean and std values for each placement
            x1 = self.extract_x_matrix_from_solution(solution[0])
            x2 = self.extract_x_matrix_from_solution(solution[1])
            mean_value_1.append(self.mean_function(x1))
            std_value_1.append(self.std_function(solution[0]))
            mean_value_2.append(self.mean_function(x2))
            std_value_2.append(self.std_function(solution[1]))
            # Save hamming distance between the placements
            hamming_dist.append(distance.hamming(u=solution[0], v=solution[1]))
            # Save frobenius distance between the placements
            frobenius_dist.append(utils.frobenius_distance(placement_pair=[s1_placement, s2_placement],
                                                           trace_WNTR_matrix=self.trace_WNTR_matrix))
        results = pd.DataFrame(list(zip(s1, s2, n1, n2,
                                        hamming_dist, frobenius_dist,
                                        mean_value_1, std_value_1,
                                        mean_value_2, std_value_2)),
                               columns=['s1', 's2', 'n1', 'n2', 'Hamming.dist', 'Frobenius.dist',
                                        'MDT1.avg', 'MDT1.std', 'MDT2.avg', 'MDT2.std'])
        print(results)
        results.to_csv('results/Net1_budget4_report.csv', index=False, sep=';')
        return results

    def remove_useless_sensors(self):
        """
        Remove sensors that never detect a contamination in any scenario.
        Remove these sensor from detection times matrix and from list of sensors.
        Let's consider that each sensors cannot detect a contamination in the scenario
        corresponding to the sensor's node.

        :return: list of indexes of sensors to remove.
        """
        sensors_to_remove = []
        for sensor_index in range(len(self.sensors)):
            count = 0
            for scenario_index in range(len(self.scenarios)):
                if self.det_times[scenario_index][sensor_index] == 0:
                    count += 1
            if count == len(self.scenarios):
                sensors_to_remove.append(sensor_index)

        # Remove sensors that never detect a contaminarion in any scenarios
        # Remove from det_times matrix
        for sensor_index in range(len(sensors_to_remove)):
            # At each iteration the matrix change dimension
            # The index will decrease every time one sensor is removed
            modified_sensor_index = sensors_to_remove[sensor_index] - sensor_index
            # Remove a sensor
            for scenario in self.det_times:
                scenario.pop(modified_sensor_index)
        # Remove from sensors list
        for index in sorted(sensors_to_remove, reverse=True):
            del self.sensors[index]
        return sensors_to_remove

    def get_sensors(self):
        return self.sensors

    def get_scenarios(self):
        return self.scenarios
