import pandas as pd
import numpy as np
import matplotlib

from utilities import utils, path


def extract_detection_times(trace_WNTR_matrix, sensor_sensibility, save_path=''):
    """
    Extract detection times for each combination scenario-sensor
    using the simulation data extracted with WNTR

    :param trace_WNTR_matrix: WNTR trace simulation results.
    :param sensor_sensibility: Level of contaminant concentracion that the considered sensors can detect.
    :param save_path: if it's different from empty string create and save data in dataframe format.
    :return matrix with rows as scenarios and columns as sensors. Each cell represent the detection time
    of this sensor in this scenario
    """

    det_times = []
    det_times_df = pd.DataFrame()

    # Extract scenarios and sensors list considered in the WNTR simulation
    # Considering the case in which sensors and scenarios are the same list of nodes
    scenarios = list(trace_WNTR_matrix.columns)[0:-2]
    sensors = list(trace_WNTR_matrix['Node'].unique())

    if save_path != '':
        det_times_df = pd.DataFrame(data={'scenario': [], 'sensor': [], 'time': []})

    for scenario_index in range(len(scenarios)):
        scenario_row = []
        for sensor_index in range(len(sensors)):
            # Get detection times for this sensor in this scenarios
            det_time_tmp = trace_WNTR_matrix[(trace_WNTR_matrix['Node'] == sensors[sensor_index]) &
                                             # Define the concentration of contaminant that sensors can detect
                                             (trace_WNTR_matrix[str(scenarios[scenario_index])] >
                                              sensor_sensibility)
                                             ]['T'].tolist()
            # Keep only the minimum detection time for this sensor/scenario combination
            if len(det_time_tmp) > 0:
                det_time_tmp = det_time_tmp[0]
            # If this sensor cannot detect the contaminant in this scenario
            # the time will be the maximum time of the simulation
            else:
                det_time_tmp = 86400  # TODO The maximum simulation time can be parametrized
            scenario_row.append(det_time_tmp)

            if save_path != '':
                det_times_df = det_times_df.append({'scenario': scenarios[scenario_index],
                                                    'sensor': sensors[sensor_index],
                                                    'time': det_time_tmp},
                                                   ignore_index=True)
        det_times.append(scenario_row)

        if save_path != '':
            det_times_df.to_csv(save_path + path.network_name + '_det_times.csv', sep=',', index=False)
            np.save(file=save_path + path.network_name + '_det_times_numpy', arr=det_times)

    return det_times


def extract_active_sensors(trace_WNTR_matrix, det_times):
    """
    Extract which sensor should be active in each scenario (the one that first detect the contaminant)
    using the det_times matrix extracted using the extract_detection_times function.
    NB: Useless considering an optimization problem like the CHAMA p-median one.

    :param trace_WNTR_matrix: WNTR trace simulation results.
    :param det_times: Detection times matrix computed with the function extract_detection_times.
    :return: matrix with rows as scenarios and columns as sensors.
    Each cell is 1 if this sensor should be active in this scenario, 0 otherwise.
    """

    # Extract scenarios and sensors list considered in the WNTR simulation
    # Considering the case in which sensors and scenarios are the same list of nodes
    scenarios = list(trace_WNTR_matrix.columns)[0:-2]
    sensors = list(trace_WNTR_matrix['Node'].unique())

    active_sensors = []
    for scenario_index in range(len(scenarios)):
        # Init the rows corrispondent to this scenario with all zeros
        scenario_row = list(np.zeros(len(sensors), dtype=np.int))
        # Reset index and value of the min
        min_index = 0
        min_det_time = 86400  # TODO The maximum simulation time can be parametrized
        for sensor_index in range(len(sensors)):
            # Take detection time corrispondent to thi scenario-sensor pair
            det_time_tmp = det_times[scenario_index][sensor_index]
            # Time is zero only when sensor and scenario are the same node
            if (det_time_tmp > 0) & (det_time_tmp < min_det_time):
                min_index = sensor_index
                min_det_time = det_time_tmp
        # Insert value 1 in the index corrisponding to the sensor
        # that first detected the contaminant in this scenario
        scenario_row[min_index] = 1
        # Append the row to the matrix
        active_sensors.append(scenario_row)
    return active_sensors


def extract_micro_array_sensor(trace_WNTR_matrix, save_path_csv='', save_path_heatmap=''):
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42
    micro_arrays = {}
    sensors = list(trace_WNTR_matrix['Node'].unique())
    for sensor in sensors:
        micro_array_tmp = trace_WNTR_matrix.loc[trace_WNTR_matrix['Node'] == sensor]
        micro_array_tmp = micro_array_tmp.drop(columns=['Node'])
        micro_array_tmp = micro_array_tmp.set_index('T')
        utils.plot_heatmap(dataframe=micro_array_tmp,
                           title='concentration_heatmap_sensor_' + str(sensor),
                           save_path=save_path_heatmap)
        micro_arrays[sensor] = micro_array_tmp
        if save_path_csv != '':
            micro_arrays[sensor].to_csv(save_path_csv + 'concentration_sensor_' + str(sensor) + '.csv',
                                        decimal=',', sep=';')
    return micro_arrays


def sum_micro_array(micro_arrays, save_path_csv='', save_path_heatmap=''):
    keys = list(micro_arrays.keys())
    micro_array_sum = micro_arrays[keys[0]].copy()
    keys.pop(0)
    for key in keys:
        micro_array_sum = micro_array_sum.add(micro_arrays[key], fill_value=0)
    utils.plot_heatmap(dataframe=micro_array_sum,
                       title='concentration_heatmap_sensor_sum', vmax=1100,
                       save_path=save_path_heatmap)
    if save_path_csv != '':
        micro_array_sum.to_csv(save_path_csv + 'concentration_sensor_sum.csv',
                               decimal=',', sep=';')
    return micro_array_sum


def extract_placement_micro_array(trace_WNTR_matrix, sensor_placement, micro_arrays=None, plot=True, save_path_csv='',
                                  save_path_heatmap=''):
    max_micro_array = {}
    if micro_arrays is None:
        micro_arrays = {}
        # Get the micro arrays regarding this placement
        for sensor in sensor_placement:
            micro_arrays[sensor] = trace_WNTR_matrix.loc[trace_WNTR_matrix['Node'] == sensor]
    # Remove node and time columns
    columns = trace_WNTR_matrix.columns.to_list()[:-2]
    # Keep only the max values between these
    for scenario in columns:
        # Init aux numpy array to compute the maximum
        max_col = np.zeros(25)
        for sensor in sensor_placement:
            # Transform the series column in a numpy array
            tmp_col = micro_arrays[sensor][scenario].to_numpy()
            # Keep only the maximum between the two numpy arrays
            max_col = np.maximum(max_col, tmp_col)
        max_micro_array[scenario] = max_col.tolist()
    # Get columns regarding Node and Time
    # max_micro_array['Node'] = micro_arrays[sensor_placement[0]]['Node'].tolist()
    max_micro_array['T'] = micro_arrays[sensor_placement[0]]['T'].tolist()
    # Create a dataframe and set the times as indexes
    micro_array_placement = pd.DataFrame(max_micro_array)
    micro_array_placement = micro_array_placement.set_index('T')
    # Plot the heatmap
    if plot:
        utils.plot_heatmap(dataframe=micro_array_placement,
                           title='concentration_heatmap_sensor_placement_' +
                                 '_'.join([str(elem) for elem in sensor_placement]),
                           save_path=save_path_heatmap)
    # Save the csv containing the micro array
    if save_path_csv != '':
        micro_array_placement.to_csv(save_path_csv + 'concentration_sensor_placement_' +
                                     '_'.join([str(elem) for elem in sensor_placement]) + '.csv',
                                     decimal=',', sep=';')
    return micro_array_placement
