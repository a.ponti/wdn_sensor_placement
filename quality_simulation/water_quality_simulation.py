import pandas as pd
import wntr
from datetime import datetime
from utilities import path

# Create a water network model
network_name = path.network_name
inp_file = path.network_dir + network_name + '.inp'
wn = wntr.network.WaterNetworkModel(inp_file)
sim = wntr.sim.EpanetSimulator(wn)
sim.run_sim(save_hyd=True)
wn.options.quality.parameter = 'TRACE'
scenario_names = wn.junction_name_list
signal_trace = pd.DataFrame()
signal_demand = pd.DataFrame()

# Simulazione di 24h, con timestep 1h
# Run trace simulations (one from each junction) and extract data needed for 
# sensor placement optimization.
print('-------------------------------------------------------')
print("start simulation time =", datetime.now())
for inj_node in scenario_names:
    # print(inj_node)
    wn.options.quality.trace_node = inj_node
    sim_results = sim.run_sim(use_hyd=True)
    trace = sim_results.node['quality']
    # trace.plot(figsize=(8, 12), subplots=True, title='Trace from node '+inj_node)
    trace = trace.stack()
    trace = trace.reset_index()
    trace.columns = ['T', 'Node', inj_node]
    # pd.set_option("display.max_rows", None, "display.max_columns", None)
    # print('trace.columns = [T, Node, inj_node]:')
    # print(trace)
    signal_trace = signal_trace.combine_first(trace)
    # save demands simulation
    demand = sim_results.node['demand']
    # demand.plot(figsize=(8, 12), subplots=True, title='Demand from simulation '+inj_node)
    demand = demand.stack()
    demand = demand.reset_index()
    demand.columns = ['T', 'Node', 'Demand']
    signal_demand = demand
# simulation data in node format for chama
signal_trace.to_csv(path.simulation_results_dir + network_name + '_trace.csv', index=False)
# demand data in node format for chama
signal_demand.to_csv(path.simulation_results_dir + network_name + '_demand.csv', index=False)
print("end simulation time =", datetime.now())
print('-------------------------------------------------------')
