import warnings
from pathlib import Path

import numpy as np  # It's necessary even it's not used
import pandas as pd

from quality_simulation.extract_data_from_WNTR import extract_micro_array_sensor, sum_micro_array
from utilities import path


def update_global_variables(network_name, n_obj, verbose, brute_force, transparent):
    path.network_name = network_name
    path.network_dir = 'networks/'
    path.simulation_results_dir = 'results/' + path.network_name + '/WNTR_simulation/'
    path.optimization_results_dir = 'results/' + path.network_name + '/optimal_sensor_placement/'
    path.singleobjective_plot_dir = 'results/' + path.network_name + '/'
    path.biobjective_plot_dir = 'results/' + path.network_name + '/biobjective_plots/'
    path.sensor_concentration_heatmap_dir = 'results/' + path.network_name + '/sensor_concentration_heatmap/'
    path.sensor_concentration_micro_arrays_dir = 'results/' + path.network_name + '/sensor_concentration_micro_arrays/'
    path.placement_micro_arrays_dir = 'results/' + path.network_name + '/placement_micro_arrays/'
    path.optimization_history_dir = 'results/' + path.network_name + '/optimization_history/'

    path.n_obj = n_obj
    path.verbose = verbose
    path.brute_force = brute_force
    path.transparent = transparent


def main():
    warnings.filterwarnings('ignore')
    # Init global variables
    update_global_variables(network_name='Neptun', n_obj=2,
                            verbose=True, brute_force=False, transparent=False)
    # Create all necessary directories
    path.create_results_dirs()

    print('#######################################################')
    print('NETWORK:', path.network_name)

    # Run the water quality simulation (only if the trace matrix is not computed yet)
    if not (Path(path.simulation_results_dir + path.network_name + '_trace.csv').is_file()):
        print('#######################################################')
        print('>>> WATER QUALITY SIMULATION <<<')
        exec(open("./quality_simulation/water_quality_simulation.py").read())

    # Run the sensor placement optimization
    print('#######################################################')
    print('>>> SENSOR PLACEMENT OPTIMIZATION <<<')
    exec(open("optimization/run_placement_optimization.py").read())

    # # Save heatmap and micro-array about sensor contaminant concentration
    # trace_matrix = pd.read_csv(path.simulation_results_dir + path.network_name + '_trace.csv', header=0, sep=',')
    # micro_arrays_sensor = extract_micro_array_sensor(trace_matrix,
    #                                                  save_path_csv=path.sensor_concentration_micro_arrays_dir,
    #                                                  save_path_heatmap=path.sensor_concentration_heatmap_dir)
    # sum_micro_array(micro_arrays_sensor,
    #                 save_path_csv=path.sensor_concentration_micro_arrays_dir,
    #                 save_path_heatmap=path.sensor_concentration_heatmap_dir)


if __name__ == "__main__":
    main()
