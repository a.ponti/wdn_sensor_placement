# Water Distribution Networks - Sensor Placement

Description

## Usage

1. Set the network name in the main.py file.
2. Set the number of objectives functions to use in the optimization in the main.py file (1 if you want only mean, 2 if you also want the variance).
3. Run the main.py file.
   
This will automatically:
1. Run the file 'water_quality_simulation.py'. The results will be two csv file (one with the trace informations and the other with demand informations).
2. Run the file 'run_pmedian_optimization.py'. This takes as input the trace informations resulting by the previous script and produce and saves the results (i.e. the optimal sensor placement for each sensor budget) in a csv file.
3. Saves the heatmap about contaminant concentration for each scenario considering each sensor.
