import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

from pathlib import Path

from quality_simulation import extract_data_from_WNTR
from utilities import path


def print_matrix(matrix):
    for row in matrix:
        print(row)


def split(a, n):
    """
    Split list a in n lists
    """

    k, m = divmod(len(a), n)
    return list(a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))


def remove_duplicates_list_of_lists(lst):
    dup_free = []
    indexes = []
    for i, x in enumerate(lst):
        if x not in dup_free:
            dup_free.append(x)
            indexes.append(i)
    return dup_free, indexes


def plot_heatmap(dataframe, title='title', vmin=0, vmax=100, save_path=''):
    plt.figure()
    ax = plt.axes(title=title)
    ax = sns.heatmap(dataframe, ax=ax, vmin=vmin, vmax=vmax, cbar_kws={'label': 'Contaminant Concentration'})
    ax.set(xlabel='Scenario', ylabel='Time')
    plt.show()
    if save_path != '':
        figure = ax.get_figure()
        figure.savefig(save_path + title, transparent=path.transparent)


def save_optimization_history(res, sensor_budget):
    save_path = path.optimization_history_dir + 'history_' + str(path.n_obj) + 'obj/'
    sensor_budget_folder = 'sensor_budget_' + str(sensor_budget) + '/'
    Path(save_path + sensor_budget_folder).mkdir(parents=True, exist_ok=True)
    file_name = path.network_name + '_' + str(path.n_obj) + 'obj_history_budget_' + str(sensor_budget)

    np.save(file=save_path + sensor_budget_folder + file_name + '_F', arr=res.pop.get('F'))
    np.save(file=save_path + sensor_budget_folder + file_name + '_X', arr=res.pop.get('X'))
    np.save(file=save_path + sensor_budget_folder + file_name + '_G', arr=res.pop.get('G'))
    np.save(file=save_path + sensor_budget_folder + file_name + '_CV', arr=res.pop.get('CV'))
    np.save(file=save_path + sensor_budget_folder + file_name + '_HV', arr=res.algorithm.callback.data['HV'])
    np.save(file=save_path + sensor_budget_folder + file_name + '_N_Pareto',
            arr=res.algorithm.callback.data['NParetoSol'])
    np.save(file=save_path + sensor_budget_folder + file_name + '_Max_Mean',
            arr=res.algorithm.callback.data['MaxMean'])
    np.save(file=save_path + sensor_budget_folder + file_name + '_Min_Mean',
            arr=res.algorithm.callback.data['MinMean'])
    np.save(file=save_path + sensor_budget_folder + file_name + '_Max_STD',
            arr=res.algorithm.callback.data['MaxSTD'])
    np.save(file=save_path + sensor_budget_folder + file_name + '_Min_STD',
            arr=res.algorithm.callback.data['MinSTD'])
    np.save(file=save_path + sensor_budget_folder + file_name + '_Max_Mean_Feasible',
            arr=res.algorithm.callback.data['MaxMeanFeasible'])
    np.save(file=save_path + sensor_budget_folder + file_name + '_Min_Mean_Feasible',
            arr=res.algorithm.callback.data['MinMeanFeasible'])
    np.save(file=save_path + sensor_budget_folder + file_name + '_Max_STD_Feasible',
            arr=res.algorithm.callback.data['MaxSTDFeasible'])
    np.save(file=save_path + sensor_budget_folder + file_name + '_Min_STD_Feasible',
            arr=res.algorithm.callback.data['MinSTDFeasible'])
    np.save(file=save_path + sensor_budget_folder + file_name + '_Edit', arr=res.algorithm.callback.data['Edit'])
    np.save(file=save_path + sensor_budget_folder + file_name + '_Frobenius', arr=res.algorithm.callback.data['Frobenius'])



def identify_pareto(scores):
    # Count number of items
    population_size = scores.shape[0]
    # Create a NumPy index for scores on the pareto front (zero indexed)
    population_ids = np.arange(population_size)
    # Create a starting list of items on the Pareto front
    # All items start off as being labelled as on the Parteo front
    pareto_front = np.ones(population_size, dtype=bool)
    # Loop through each item. This will then be compared with all other items
    for i in range(population_size):
        # Loop through all other items
        for j in range(population_size):
            # Check if our 'i' pint is dominated by out 'j' point
            if all(scores[j] <= scores[i]) and any(scores[j] < scores[i]):
                # j dominates i. Label 'i' point as not on Pareto front
                pareto_front[i] = 0
                # Stop further comparisons with 'i' (no more comparisons needed)
                break
    # Return ids of scenarios on pareto front
    return population_ids[pareto_front]


def binary_vector_to_placement(points, sensors):
    placements = []
    for solution in points:
        sensor_indexes = [i for i, sensor in enumerate(solution) if sensor]
        placements.append(np.array(sensors)[sensor_indexes].tolist())
    return placements


def binary_solution_to_placement(point, sensors):
    sensor_indexes = [i for i, sensor in enumerate(point) if sensor]
    return np.array(sensors)[sensor_indexes].tolist()


def normalize(values):
    max_value = max(values)
    min_value = min(values)
    norm_values = [(float(i) - min_value) / (max_value - min_value) for i in values]
    return norm_values


def frobenius_distance(placement_pair, trace_WNTR_matrix, micro_arrays=None):
    scenarios = list(trace_WNTR_matrix.columns)[0:-2]

    if len(placement_pair[0]) != 0:
        # Get the micro array of this placement
        micro_array_1 = extract_data_from_WNTR.extract_placement_micro_array(trace_WNTR_matrix, placement_pair[0],
                                                                             micro_arrays=micro_arrays, plot=False)
        # Convert pandas dataframe in numpy 2D array
        micro_array_1 = micro_array_1.to_numpy()
    else:
        # If the placement is empty (no sensors are placed) the micro array matrix is full of 86400
        # (the maximum simulation time)
        # TODO 25 is the number of time step and it should be parametrized
        micro_array_1 = np.full(shape=(25, len(scenarios)), fill_value=0)

    if len(placement_pair[1]) != 0:
        # Get the micro array of this placement
        micro_array_2 = extract_data_from_WNTR.extract_placement_micro_array(trace_WNTR_matrix, placement_pair[1],
                                                                             micro_arrays=micro_arrays, plot=False)
        # Convert pandas dataframe in numpy 2D array
        micro_array_2 = micro_array_2.to_numpy()
    else:
        # If the placement is empty (no sensors are placed) the micro array matrix is full of 86400
        # (the maximum simulation time)
        micro_array_2 = np.full(shape=(25, len(scenarios)), fill_value=0)
    # Compute the Frobenius distance between this two matrix
    difference = micro_array_1 - micro_array_2
    return np.linalg.norm(difference, ord='fro')


def plot_hypervolume_lines(optimal_mean, optimal_std, line_color='grey', line_width=2, ref_point=(86400, 86400 / 2)):
    min_mean = min(optimal_mean)
    max_mean = max(optimal_mean)
    min_std = min(optimal_std)
    max_std = max(optimal_std)

    # Line for std
    x_coords = [min_mean, min_mean]
    y_coords = [max_std, ref_point[1]]
    line1 = plt.plot(x_coords, y_coords, color=line_color, linewidth=line_width)
    # Line for mean
    x_coords = [max_mean, ref_point[0]]
    y_coords = [min_std, min_std]
    line2 = plt.plot(x_coords, y_coords, color=line_color, linewidth=line_width)
    # Line to close the polygon
    x_coords = [min_mean, ref_point[0]]
    y_coords = [ref_point[1], ref_point[1]]
    line3 = plt.plot(x_coords, y_coords, color=line_color, linewidth=line_width)
    x_coords = [ref_point[0], ref_point[0]]
    y_coords = [min_std, ref_point[1]]
    line4 = plt.plot(x_coords, y_coords, color=line_color, linewidth=line_width)
