from pathlib import Path

# GLOBAL VARIABLES
network_name = 'Net1'
n_obj = 2
verbose = True
brute_force = False
transparent = False
# PATHS
network_dir = 'networks/'
simulation_results_dir = 'results/' + network_name + '/WNTR_simulation/'
optimization_results_dir = 'results/' + network_name + '/optimal_sensor_placement/'
singleobjective_plot_dir = 'results/' + network_name + '/'
biobjective_plot_dir = 'results/' + network_name + '/biobjective_plots/'
sensor_concentration_heatmap_dir = 'results/' + network_name + '/sensor_concentration_heatmap/'
sensor_concentration_micro_arrays_dir = 'results/' + network_name + '/sensor_concentration_micro_arrays/'
placement_micro_arrays_dir = 'results/' + network_name + '/placement_micro_arrays/'
optimization_history_dir = 'results/' + network_name + '/optimization_history/'

def create_results_dirs():
    Path('results/' + network_name).mkdir(parents=True, exist_ok=True)
    Path(simulation_results_dir).mkdir(parents=True, exist_ok=True)
    Path(optimization_results_dir).mkdir(parents=True, exist_ok=True)
    Path(singleobjective_plot_dir).mkdir(parents=True, exist_ok=True)
    Path(biobjective_plot_dir).mkdir(parents=True, exist_ok=True)
    Path(sensor_concentration_heatmap_dir).mkdir(parents=True, exist_ok=True)
    Path(sensor_concentration_micro_arrays_dir).mkdir(parents=True, exist_ok=True)
    Path(placement_micro_arrays_dir).mkdir(parents=True, exist_ok=True)
    Path(optimization_history_dir).mkdir(parents=True, exist_ok=True)
